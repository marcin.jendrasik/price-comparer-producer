package com.pricecomparer.producer.service.crawler;

import com.pricecomparer.model.domain.Product;
import com.pricecomparer.producer.persistence.repository.ProductRepository;
import com.pricecomparer.producer.service.producer.ProductAptekaGeminiAsyncDelegatorService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ProductAptekaGeminiServiceTest {

    @MockBean
    private ProductAptekaGeminiAsyncDelegatorService productAptekaGeminiDelegatorService;

    @Autowired
    private ProductAptekaGeminiService productAptekaGeminiService;

    @Autowired
    private ProductRepository productRepository;

    @Test
    void whenDataCrawledAndSaved_thenPagesAreNotEmpty() {
        //when
        productAptekaGeminiService.crawlAndSave(1, 1);
        final Page<Product> pages = productRepository.getAll(false, PageRequest.of(0, Integer.MAX_VALUE));

        //then
        assertFalse(pages.isEmpty());
    }
}