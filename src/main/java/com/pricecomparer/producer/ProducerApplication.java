package com.pricecomparer.producer;

import com.pricecomparer.producer.service.producer.ProductAptekaGeminiAsyncDelegatorService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class ProducerApplication implements CommandLineRunner {

    private final ProductAptekaGeminiAsyncDelegatorService productAptekaGeminiDelegatorService;

    public static void main(String[] args) {
        SpringApplication.run(ProducerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        productAptekaGeminiDelegatorService.process();
    }
}
