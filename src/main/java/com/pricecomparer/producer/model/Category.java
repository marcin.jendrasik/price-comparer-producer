package com.pricecomparer.producer.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Category {

    private Long id;
    private String name;
    private String link;
}
