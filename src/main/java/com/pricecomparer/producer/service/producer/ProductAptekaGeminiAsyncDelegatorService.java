package com.pricecomparer.producer.service.producer;

import com.pricecomparer.producer.service.crawler.ProductAptekaGeminiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductAptekaGeminiAsyncDelegatorService {

    private final ProductAptekaGeminiService productAptekaGeminiService;

    @Async
    public void process() {
        productAptekaGeminiService.crawlAndSaveAll();
    }
}
