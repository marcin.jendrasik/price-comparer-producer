package com.pricecomparer.producer.service.producer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
@RequiredArgsConstructor
@Slf4j
public abstract class AbstractProducerService<K, V> {

    protected final KafkaTemplate<K, V> kafkaTemplate;

    protected void send(final String topic, final V value) {
        ListenableFuture<SendResult<K, V>> future = kafkaTemplate.send(topic, value);

        future.addCallback(new ListenableFutureCallback<>() {

            @Override
            public void onSuccess(final SendResult<K, V> result) {
                log.info("Sent value=[{}] with offset=[{}]", value, result.getRecordMetadata().offset());
            }
            @Override
            public void onFailure(final Throwable ex) {
                log.error("Unable to send value=[{}] due to : {}", value, ex.getMessage());
            }
        });
    }
}
