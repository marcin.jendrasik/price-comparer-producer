package com.pricecomparer.producer.service.producer;

import com.pricecomparer.model.avro.Product;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ProductProducerService extends AbstractProducerService<String, Product> {

    public ProductProducerService(KafkaTemplate<String, Product> kafkaTemplate) {
        super(kafkaTemplate);
    }

    public void send(final String topic, final Product product) {
        super.send(topic, product);
    }
}
