package com.pricecomparer.producer.service.cron;

import com.pricecomparer.producer.config.KafkaConfigTopicData;
import com.pricecomparer.producer.mapper.ProductMapper;
import com.pricecomparer.producer.service.crawler.ProductAptekaGeminiService;
import com.pricecomparer.producer.service.producer.ProductProducerService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductProducerCronService {

    public static final int BATCH_SIZE = 10;
    private final KafkaConfigTopicData kafkaConfigTopicData;
    private final ProductProducerService productProducerService;
    private final ProductAptekaGeminiService productAptekaGeminiService;

    @Scheduled(fixedDelay = 5000)
    public void process() {
        productAptekaGeminiService.getAll(false, PageRequest.of(0, BATCH_SIZE))
            .getContent()
            .forEach(product -> {
                productProducerService.send(kafkaConfigTopicData.getCreateProduct(), ProductMapper.toAvro(product));
                product.setConsumed(true);
                productAptekaGeminiService.save(product);
            });
    }
}
