package com.pricecomparer.producer.service.cron;

import com.pricecomparer.producer.service.crawler.ProductAptekaGeminiService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductCronService {

    private final ProductAptekaGeminiService productAptekaGeminiService;

    @Scheduled(cron = "${scheduler.create-product.cron}")
    public void send() {
        productAptekaGeminiService.crawlAndSaveAll();
    }
}
