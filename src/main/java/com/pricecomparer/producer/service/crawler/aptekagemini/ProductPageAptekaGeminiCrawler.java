package com.pricecomparer.producer.service.crawler.aptekagemini;

import com.pricecomparer.producer.model.AptekaGeminiConf;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;
import com.pricecomparer.model.domain.Product;
import com.pricecomparer.model.domain.ProductType;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProductPageAptekaGeminiCrawler {

    public Set<Product> crawl(final String url) {
        Document document;
        try {
            log.info("Getting products from: {}", url);
            document = Jsoup.connect(url).get();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Elements items = document.select(".justify-items-center article");
        return items.stream()
                .filter(Objects::nonNull)
                .map(item -> Product.builder()
                        .img(Optional.ofNullable(item.selectFirst("img"))
                                .map(i -> i.attr("data-src"))
                            .orElse(null))
                        .title(Optional.ofNullable(item.selectFirst(".line-clamp-2"))
                                .map(Element::text)
                            .orElse(null))
                        .link(Optional.ofNullable(item.selectFirst("a"))
                                .map(i -> AptekaGeminiConf.URL + i.attr("href"))
                            .orElse(null))
                        .price(Optional.ofNullable(item.selectFirst("span.text-16"))
                                .map(Element::text)
                            .orElse(null))
                        .type(ProductType.APTEKA_GEMINI)
                        .build())
                .collect(Collectors.toSet());
    }
}
