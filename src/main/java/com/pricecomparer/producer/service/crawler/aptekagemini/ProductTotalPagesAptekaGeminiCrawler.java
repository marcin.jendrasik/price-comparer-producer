package com.pricecomparer.producer.service.crawler.aptekagemini;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;

@Service
@Slf4j
public class ProductTotalPagesAptekaGeminiCrawler {

    public Integer crawl(final String url) {
        Document document;
        try {
            log.info("Getting count of pages from: {}", url);
            document = Jsoup.connect(url).get();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return Optional.ofNullable(document.select("[data-ga-id='bottom_pagination_button_number'].cursor-pointer > span"))
                .map(Elements::text)
                .map(Integer::valueOf)
                .orElse(null);
    }
}
