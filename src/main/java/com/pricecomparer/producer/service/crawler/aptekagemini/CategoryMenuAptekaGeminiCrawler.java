package com.pricecomparer.producer.service.crawler.aptekagemini;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;
import com.pricecomparer.producer.model.AptekaGeminiConf;
import com.pricecomparer.producer.model.Category;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CategoryMenuAptekaGeminiCrawler {

    public List<Category> crawl() {
        return getCategories();
    }

    private List<Category> getCategories() {
        Document document;
        try {
            log.info("Getting menu categories from: {}", AptekaGeminiConf.URL);
            document = Jsoup.connect(AptekaGeminiConf.URL).get();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return document.select(".flex > .megamenu-link-desktop")
                .stream()
                .map(a -> Category.builder()
                        .name(a.text())
                        .link(a.attr("href"))
                        .build())
                .collect(Collectors.toList());
    }
}
