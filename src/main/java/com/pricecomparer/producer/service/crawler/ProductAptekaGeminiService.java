package com.pricecomparer.producer.service.crawler;

import com.pricecomparer.model.domain.Product;
import com.pricecomparer.producer.model.AptekaGeminiConf;
import com.pricecomparer.producer.model.Category;
import com.pricecomparer.producer.persistence.repository.ProductRepository;
import com.pricecomparer.producer.service.crawler.aptekagemini.CategoryMenuAptekaGeminiCrawler;
import com.pricecomparer.producer.service.crawler.aptekagemini.ProductPageAptekaGeminiCrawler;
import com.pricecomparer.producer.service.crawler.aptekagemini.ProductTotalPagesAptekaGeminiCrawler;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

@Service
@RequiredArgsConstructor
public class ProductAptekaGeminiService {

    private static final String PAGE_PATTERN = "?page=";
    private final CategoryMenuAptekaGeminiCrawler categoryMenuAptekaGeminiCrawler;
    private final ProductTotalPagesAptekaGeminiCrawler productTotalPagesAptekaGeminiCrawler;
    private final ProductPageAptekaGeminiCrawler productPageAptekaGeminiCrawler;
    private final ProductRepository productRepository;

    public void crawlAndSaveAll() {
        crawlAndSave(Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

    public void crawlAndSave(int pageToSave, int categoryToSave) {
        final List<Category> categories = categoryMenuAptekaGeminiCrawler.crawl();
        categories.subList(0, Math.min(categories.size(), categoryToSave)).forEach(category -> {
            String url = AptekaGeminiConf.URL + category.getLink();
            Integer pages = productTotalPagesAptekaGeminiCrawler.crawl(url);
            pages = pages != null ? Math.min(pages, pageToSave) : null;
            if (Objects.nonNull(pages)) {
                IntStream.rangeClosed(1, pages)
                        .forEach(page -> productPageAptekaGeminiCrawler.crawl(url + PAGE_PATTERN + page)
                                .forEach(this::save));
            } else {
                productPageAptekaGeminiCrawler.crawl(url)
                        .forEach(productRepository::save);
            }
        });
    }

    @Transactional
    public void save(final Product product) {
        productRepository.save(product);
    }

    @Transactional(readOnly = true)
    public Page<Product> getAll(final Boolean consumed, final Pageable pageable) {
        return productRepository.getAll(consumed, pageable);
    }
}
