package com.pricecomparer.producer.mapper;

import com.pricecomparer.model.avro.ProductType;
import lombok.experimental.UtilityClass;
import com.pricecomparer.model.domain.Product;
import com.pricecomparer.producer.persistence.entity.ProductEntity;

import java.util.Optional;

@UtilityClass
public class ProductMapper {

    public static Product toDomain(final ProductEntity productEntity) {
        return Optional.ofNullable(productEntity)
                .map(pe -> Product.builder()
                        .id(pe.getId())
                        .title(pe.getTitle())
                        .img(pe.getImg())
                        .link(pe.getLink())
                        .price(pe.getPrice())
                        .consumed(pe.isConsumed())
                        .type(pe.getType())
                        .build())
                .orElse(null);
    }

    public static com.pricecomparer.model.avro.Product toAvro(final Product product) {
        return Optional.ofNullable(product)
                .map(p -> {
                    com.pricecomparer.model.avro.Product avro = new com.pricecomparer.model.avro.Product();
                    avro.setId(p.getId());
                    avro.setTitle(p.getTitle());
                    avro.setImg(p.getImg());
                    avro.setLink(p.getLink());
                    avro.setPrice(p.getPrice());
                    avro.setType(ProductType.valueOf(p.getType().name()));
                    return avro;
                })
                .orElse(null);
    }

    public static ProductEntity toEntity(final Product product) {
        return Optional.ofNullable(product)
                .map(p -> ProductEntity.builder()
                        .id(p.getId())
                        .title(p.getTitle())
                        .img(p.getImg())
                        .link(p.getLink())
                        .price(p.getPrice())
                        .consumed(p.isConsumed())
                        .type(p.getType())
                        .build())
                .orElse(null);
    }
}
