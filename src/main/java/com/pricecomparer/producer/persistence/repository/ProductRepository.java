package com.pricecomparer.producer.persistence.repository;

import com.pricecomparer.model.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductRepository {

    void save(Product product);
    Page<Product> getAll(Boolean consumed, Pageable pageable);
}
