package com.pricecomparer.producer.persistence.repository;

import com.pricecomparer.producer.persistence.entity.ProductEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductJpaRepository extends JpaRepository<ProductEntity, Long> {

    Page<ProductEntity> findAllByConsumed(boolean consumed, Pageable pageable);
}
