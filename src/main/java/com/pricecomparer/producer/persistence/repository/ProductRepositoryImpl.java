package com.pricecomparer.producer.persistence.repository;

import com.pricecomparer.model.domain.Product;
import com.pricecomparer.producer.mapper.ProductMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class ProductRepositoryImpl implements ProductRepository {

    private final ProductJpaRepository productJpaRepository;

    @Override
    public void save(final Product product) {
        productJpaRepository.save(ProductMapper.toEntity(product));
    }

    @Override
    public Page<Product> getAll(final Boolean consumed, final Pageable pageable) {
        if (consumed != null) {
            return productJpaRepository.findAllByConsumed(consumed, pageable)
                    .map(ProductMapper::toDomain);
        }

        return productJpaRepository.findAll(pageable)
                .map(ProductMapper::toDomain);
    }
}
