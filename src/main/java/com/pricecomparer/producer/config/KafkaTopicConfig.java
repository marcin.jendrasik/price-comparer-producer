package com.pricecomparer.producer.config;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.HashMap;
import java.util.Map;

@Configuration
@RequiredArgsConstructor
public class KafkaTopicConfig {

    private final KafkaConfigData kafkaConfigData;
    private final KafkaConfigTopicData kafkaConfigTopicData;

    @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaConfigData.getBootstrapAddress());
        return new KafkaAdmin(configs);
    }

    @Bean
    public NewTopic createProductTopic() {
        return new NewTopic(kafkaConfigTopicData.getCreateProduct(),
                kafkaConfigTopicData.getNumOfPartitions(),
                kafkaConfigTopicData.getReplicationFactor());
    }
}