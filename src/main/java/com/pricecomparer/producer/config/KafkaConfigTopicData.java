package com.pricecomparer.producer.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "kafka-topic-config")
public class KafkaConfigTopicData {

    private String createProduct;
    private Integer numOfPartitions;
    private Short replicationFactor;
}
