package com.pricecomparer.model.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Product {

    private Long id;
    private String title;
    private String img;
    private String price;
    private String link;
    private ProductType type;
    private boolean consumed;

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", img='" + img + '\'' +
                ", price='" + price + '\'' +
                ", link='" + link + '\'' +
                ", type=" + type +
                ", consumed=" + consumed +
                '}';
    }
}