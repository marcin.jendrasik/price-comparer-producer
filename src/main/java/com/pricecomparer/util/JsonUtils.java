package com.pricecomparer.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

public class JsonUtils {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    public static String prettifyJson(String jsonString) {
        try {
            Object jsonObject = MAPPER.readValue(jsonString, Object.class);
            ObjectWriter writer = MAPPER.writer().with(SerializationFeature.INDENT_OUTPUT);
            return writer.writeValueAsString(jsonObject);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String addFieldToJson(String jsonString, String fieldName, String fieldValue) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(jsonString);
            ((com.fasterxml.jackson.databind.node.ObjectNode) jsonNode).put(fieldName, fieldValue);
            return mapper.writeValueAsString(jsonNode);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
