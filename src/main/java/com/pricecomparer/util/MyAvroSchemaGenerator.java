package com.pricecomparer.util;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.dataformat.avro.AvroMapper;
import com.fasterxml.jackson.dataformat.avro.AvroSchema;
import com.fasterxml.jackson.dataformat.avro.jsr310.AvroJavaTimeModule;
import com.fasterxml.jackson.dataformat.avro.schema.AvroSchemaGenerator;
import org.apache.avro.Schema;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class MyAvroSchemaGenerator {

    private MyAvroSchemaGenerator() {}

    public static void createAvroSchemaFromClass(Path path, String namespace, Class<?>... clazz) {
        for (Class<?> aClass : clazz) {
            createAvroSchemaFromClass(path, namespace, aClass);
        }
    }

    public static void createAvroSchemaFromClass(Path path, String namespace, Class<?> clazz) {
        try {
            Files.createDirectories(path);

            AvroMapper avroMapper = AvroMapper.builder()
                    .disable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY)
                    .addModule(new AvroJavaTimeModule())
                    .build();

            AvroSchemaGenerator gen = new AvroSchemaGenerator();
            gen.enableLogicalTypes();
            avroMapper.acceptJsonFormatVisitor(clazz, gen);
            AvroSchema schemaWrapper = gen.getGeneratedSchema();

            Schema avroSchema = schemaWrapper.getAvroSchema();
            String avroSchemaInJSON = avroSchema.toString(true);

            String avroWithNamespace = JsonUtils.prettifyJson(JsonUtils.addFieldToJson(avroSchemaInJSON, "namespace", namespace));

            System.out.println(avroWithNamespace);

            //Write to File
            Path fileName = Path.of(path.toString(), clazz.getSimpleName() + ".avsc");
            Files.writeString(fileName, avroWithNamespace);
            System.out.println("Avro schema generated successfully for class: " + clazz.getSimpleName() + ", destination is: " + fileName);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
