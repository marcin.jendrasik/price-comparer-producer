package com.pricecomparer.util;

import com.pricecomparer.model.domain.Product;
import com.pricecomparer.model.domain.ProductType;

import java.nio.file.Path;

public class AvroSchemaInvoker {

    public static void main(String[] args) {
        MyAvroSchemaGenerator.createAvroSchemaFromClass(
                Path.of("C:\\Users\\mjendrasik\\IdeaProjects\\a_own_project\\price-comparer-producer\\src\\main\\resources\\avro"),
                "com.pricecomparer.model.avro",
                Product.class,
                ProductType.class);
    }
}
