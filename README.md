# Price Comparer Producer
This microservice app is one of the parts of Price Comparer app. Price Comparer consists of:

- Price Comparer Producer
- [Price Comparer Consumer](https://gitlab.com/marcin.jendrasik/price-comparer-consumer)

## Introduction
This microservice will be responsible for crawl pharmacies sites in order to getting all base information about products and send them to Kafka.
Then another micorservice [Price Comparer Consumer](https://gitlab.com/marcin.jendrasik/price-comparer-consumer) will fetch these data and use them in search-engine. 
The process of crawl sites will start after run app.

## Technologies
- OpenJDK v11.0.5
- Spring Boot Framework v2.3.4
- Zookeeper/Kafka
- Postgres v10.5
- Minikube v1.31.2
- Kubernetesa v1.27.4
- Docker v24.0.4
- Docker Compose v1.21.2

## Prerequisites
- Familiar with docker and docker-compose
- Familiar with Minikube or Kubernetes
- Helm v3.4.0
- Minikube v1.31.2
- Docker v24.0.4
- Docker Compose v1.21.2

## Cron
The application includes a Cron worker which will run at 1 am to update data.

## Run applications by docker-compose
> :warning:  **Run Price Comparer app (Consumer, Producer) by one command by docker-compose.**

1.Go to the "docker-compose" folder.
```bash
cd ./docker-compose/
```
2.Run with detach mode.
```bash
docker-compose up --build -d
```
3.Check all containers are up and running "STATUS up...". That can take long time, approximately 15 minutes until all services have been started.
```bash
docker ps
```
4.Go to the products page in the browser and happy searching 😄

http://localhost:8085/products

5.Shutting down the apps by writing in the "docker-compose" folder.
```bash
docker-compose down
```

## Price Comparer Chart Repository
You can find [here](https://github.com/MartinMartinni/price-comparer-charts).

## Run applications on Minikube cluster
> :warning:  **Run Price Comparer app (Consumer, Producer) on Minikube cluster by commands below.**

1.Start a local Kubernetes cluster with requirements. To increase efficiency add more: memory or cpus.
```bash
minikube start --driver=docker --memory=5000 --cpus=4
```
2.Add a chart repository.
```bash
helm repo add price-comparer https://martinmartinni.github.io/price-comparer-charts
```
3.Update information of available charts locally from chart repositories.
```bash
helm repo update
```
4.Run apps from chart repository.
```bash
helm install pc1 price-comparer/producer
```
5.Check all pods are up and running. That can take long time, approximately 15 minutes, until all services achieve a state of "READY 1/1". For better performance give more RAMs and CPUs. Producer will be always in status "READY 0/1" until all the data has been processed.
```bash
kubectl get pods -w
```
6.To provide a routing to the app you need to create a tunnel. Write in the new terminal and don't close it :warning:
```bash
minikube tunnel
```
7.Go to the products page in the browser and happy searching :smile:

http://localhost:8085/products

8.Shutting down the apps
```bash
helm uninstall pc1
```
close "minikube tunnel" terminal

> :warning: **Sometimes you could need to remove some resources when apps don't want to start. After that try again.**
```bash
kubectl delete serviceaccounts --all
kubectl delete secrets --all
kubectl delete configmaps --all
kubectl delete services --all
kubectl delete statefulsets.apps --all
kubectl delete pvc --all
```

